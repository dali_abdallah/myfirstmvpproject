# Simple MVP Project #

MVP (Model-View-Presenter) pattern is a derivative from the well known MVC (Model-View-Controller). It allows separation of the presentation layer from the logic, so that everything about how the interface works is separated from how we represent it on screen. Ideally the MVP pattern would achieve that same logic might have completely different and interchangeable views.

you can see my article about MVP pattern for Android via this [Link](http://norappweb.net:8090/display/TEC/MVP+design+pattern+for+Android)

Hopefully, you find anything you need here about MVP implementation.


Email: mohamed.ali.abdallah@norappweb.com